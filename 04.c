#include <stdio.h>
#include <stdlib.h>

int main(){
    int area_code;
    printf("Enter the area code to find the area: ");
    scanf("%d",&area_code);
    switch(area_code){
        case 229: printf("\nArea code %d is using in Albany\n",area_code);break;
        case 404: printf("\nArea code %d is using in Atlanta\n",area_code);break;
        case 478: printf("\nArea code %d is using in Macon\n",area_code);break;
        case 706: printf("\nArea code %d is using in Columbus\n",area_code);break;
        case 912: printf("\nArea code %d is using in Savannah\n",area_code);break;
        default: printf("\nArea code not recognized\n");break;
    }
    return 0;
}
