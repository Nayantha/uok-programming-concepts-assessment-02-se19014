#include <stdio.h>
#include <stdlib.h>

int main(){
    int temperaturesInC,temperaturesInF;
    printf("\nTable of centigrade temperatures 0 to 50 and their Fahrenheit equivalents\n\n C |  F\n");
    for(temperaturesInC=0;temperaturesInC<=50;temperaturesInC++){
        temperaturesInF = (temperaturesInC *9/5)-32;
        printf("%2d | %2d \n",temperaturesInC,temperaturesInF);
    }
    return 0;
}

