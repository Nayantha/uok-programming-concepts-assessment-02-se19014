#include <stdio.h>
#include <stdlib.h>
int main()
{   int marks[4],i,j;
    char grade[4],subject[4][12] = {"Physics", "Chemistry", "Mathematics", "English"};
    for (i=0;i<4;i++){
        printf("Marks %s: ",subject[i]);
        scanf("%d",&marks[i]);
        if (100>= marks[i] && marks[i] >=90){grade[i]='A';}
        else if (90> marks[i] && marks[i] >=80){grade[i]='B';}
        else if (80> marks[i] && marks[i] >=70){grade[i]='C';}
        else if (70> marks[i] && marks[i] >=60){grade[i]='D';}
        else if (60> marks[i] && marks[i] >=40){grade[i]='E';}
        else if (0<=marks[i] && marks[i] <40){grade[i]='F';}
        else{printf("Wrong Entry");}}
    printf("\t|\tMarks |\tGrade");
    for (i=0;i<4;i++){
        printf("\n%s|\t%3d  |\t%c",subject[i],marks[i],grade[i]);}
    return 0;}
