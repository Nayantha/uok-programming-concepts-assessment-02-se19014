#include <stdio.h>
#include <stdlib.h>

int main(){
    int firstNum=0,secondNum=1,i,nextNum,endLimit;
    printf("How long do you want want 'Fibonacci series': ");
    scanf("%d",&endLimit);
    nextNum = firstNum + secondNum;
    printf(" 1 : %2d\n 2 : %2d\n 3 : %2d\n",firstNum,secondNum,nextNum);
    for (i=4;i<=endLimit;i++){
        firstNum = secondNum;
        secondNum = nextNum;
        nextNum = firstNum + secondNum;
         printf("%2d : %2d\n",i, nextNum);
    }
    return 0;
}
